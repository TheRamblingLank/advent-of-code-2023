#include <fstream>
#include <iostream>
#include <string>
#include <map>

const static char NEWLINE = 10;

struct FoundDigit
{
    char digit;
    int index;

    FoundDigit()
        : digit(0)
        , index(-1){};

    void reset() {
        digit = 0;
        index = -1;
    }
};

const static std::map<std::string, char> numberStringToIntMap = {
        {"one", '1'},
        {"two", '2'},
        {"three", '3'},
        {"four", '4'},
        {"five", '5'},
        {"six", '6'},
        {"seven", '7'},
        {"eight", '8'},
        {"nine", '9'}
};

int main()
{
    std::fstream fIn("../input.txt", std::fstream::in);

    char currentChar;
    int total = 0;
    int colIndex = 0;
    int lineNumber = 0;
    FoundDigit firstCharDigit;
    FoundDigit lastCharDigit;
    std::string charBuffer;

    while (fIn >> std::noskipws >> currentChar)
    {
        // Char number
        if (isdigit(currentChar))
        {
            if (!firstCharDigit.digit)
            {
                firstCharDigit.digit = currentChar;
                firstCharDigit.index = colIndex;
            }
            lastCharDigit.digit = currentChar;
            lastCharDigit.index = colIndex;
        }
        // \n char
        if (currentChar == NEWLINE)
        {
            FoundDigit firstStringOccurrence;
            FoundDigit lastStringOccurrence;
            for (auto it = numberStringToIntMap.begin(); it != numberStringToIntMap.end(); ++it)
            {
                std::size_t position = charBuffer.find(it->first);
                if (position != std::string::npos && (firstStringOccurrence.index == -1 || position < firstStringOccurrence.index))
                {
                    firstStringOccurrence.digit = it->second;
                    firstStringOccurrence.index = static_cast<int>(position);
                }

                position = charBuffer.rfind(it->first);
                if (position != std::string::npos && (lastStringOccurrence.index == -1 || position > lastStringOccurrence.index))
                {
                    lastStringOccurrence.digit = it->second;
                    lastStringOccurrence.index = static_cast<int>(position);
                }
            }
            char firstDigit;
            char lastDigit;

            if (firstStringOccurrence.index == -1)
            {
                firstDigit = firstCharDigit.digit;
            } else
            {
                firstCharDigit.index < firstStringOccurrence.index ? firstDigit = firstCharDigit.digit : firstDigit = firstStringOccurrence.digit;
            }

            if (lastStringOccurrence.index == -1)
            {
                lastDigit = lastCharDigit.digit;
            } else
            {
                lastCharDigit.index > lastStringOccurrence.index ? lastDigit = lastCharDigit.digit : lastDigit = lastStringOccurrence.digit;
            }

            total += std::stoi(std::string(1, firstDigit) + lastDigit);

            firstCharDigit.reset();
            lastCharDigit.reset();
            charBuffer.clear();
            colIndex = -1;
        }
        // Add any chars into the line stored in memory
        else
        {
            charBuffer += currentChar;
        }
        colIndex++;
    }
    std::cout << "Total: " << total;
}